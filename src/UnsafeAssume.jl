# Copyright © 2023 Neven Sajko. All rights reserved.

module UnsafeAssume

export
  unsafe_assume_this_call_is_unreachable, unsafe_assume_condition,
  unsafe_assume_integer_is_divisible_by_power_of_two,
  unsafe_assume_pointer_is_aligned

@static if VERSION < v"1.8-"
  macro unreachable_effects(x)
    :($x)
  end
elseif VERSION < v"1.9-"
  macro unreachable_effects(x)
    :(Base.@assume_effects :nothrow $x)
  end
else
  macro unreachable_effects(x)
    :(Base.@assume_effects :nothrow :notaskstate :inaccessiblememonly $x)
  end
end

"""
    unsafe_assume_this_call_is_unreachable()

LLVM will assume that any call of this function will *never* be reached in any
execution of your code. In other words, the call is assumed to be *unreachable*.

!!! danger
    Lying to the compiler is **dangerous**, convincing LLVM that a false statement
    is true can break your program in subtle ways.
"""
@unreachable_effects @inline function unsafe_assume_this_call_is_unreachable()
  @inline begin
    # https://llvm.org/docs/LangRef.html#unreachable-instruction
    Core.Intrinsics.llvmcall("unreachable", Union{}, Tuple{})
  end
end

"""
    unsafe_assume_condition(::Bool)

LLVM will assume that the provided argument will be `true`, in any possible
execution of your program. Returns `nothing`.

!!! danger
    Lying to the compiler is **dangerous**, convincing LLVM that a false statement
    is true can break your program in subtle ways.
"""
@inline function unsafe_assume_condition(c::Bool)
  @inline begin
    # TODO: implement using LLVM's `llvm.assume` intrinsic directly?
    # https://llvm.org/docs/LangRef.html#llvm-assume-intrinsic
    c || unsafe_assume_this_call_is_unreachable()
    nothing
  end
end

function integer_is_divisible_by_power_of_two(n::Integer, ::Val{mask}) where {mask}
  @inline begin
    mask::Unsigned
    T = typeof(mask)
    rough = n % T
    finer = rough & mask
    iszero(finer)::Bool
  end
end

"""
    unsafe_assume_integer_is_divisible_by_power_of_two(n::Integer, mask::Val)

Method signature/prototype: `(n::Integer, ::Val{mask})::Nothing where {mask}`.

LLVM will assume that the provided integer `n` has its least significant bits
cleared, as specified by the unsigned value `mask`. The latter is passed in the type
domain, using `Val`.

!!! danger
    Lying to the compiler is **dangerous**, convincing LLVM that a false statement
    is true can break your program in subtle ways.
"""
@inline function unsafe_assume_integer_is_divisible_by_power_of_two(n::Integer, mask::Val)
  @inline begin
    is_round = integer_is_divisible_by_power_of_two(n, mask)
    unsafe_assume_condition(is_round)
  end
end

"""
    unsafe_assume_pointer_is_aligned(p::Ptr, mask::Val)

Method signature/prototype: `(p::Ptr, ::Val{mask})::Nothing where {mask}`.

LLVM will assume that the provided pointer `p` has its least significant bits
cleared, as specified by the unsigned value `mask`. The latter is passed in the type
domain, using `Val`.

!!! danger
    Lying to the compiler is **dangerous**, convincing LLVM that a false statement
    is true can break your program in subtle ways.
"""
@inline function unsafe_assume_pointer_is_aligned(p::Ptr, mask::Val)
  @inline begin
    n = reinterpret(UInt, p)
    unsafe_assume_integer_is_divisible_by_power_of_two(n, mask)
  end
end

end
