using Test, UnsafeAssume

const integer_is_divisible_by_power_of_two = UnsafeAssume.integer_is_divisible_by_power_of_two

function assumption_return_types(fun::Fun, ::Type{Ret}, arg_types::Tuple) where {Fun, Ret}
  @test all((T -> T == Ret), Base.return_types(fun, Tuple{arg_types...}))
end

assumption_return_types(fun::Fun, arg_types::Tuple) where {Fun} =
  assumption_return_types(fun, Nothing, arg_types)

const SignedIntegerTypes = (Int8, Int16, Int32, Int64)
const UnsignedIntegerTypes = map(unsigned, SignedIntegerTypes)
const IntegerTypes = (SignedIntegerTypes..., UnsignedIntegerTypes...)

const PointerTypes = map((I -> Ptr{I}), IntegerTypes)

const masks_8 = (0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f, 0x7f, 0xff)::NTuple{8, UInt8}
const masks_16 = map(UInt16, masks_8)
const masks_32 = map(UInt32, masks_8)
const masks = (masks_8..., masks_16..., masks_32...)
const Masks = map((m -> Val{m}), masks)

const some_odd_integers = (collect(1:2:31)...,)

function unsafe_sqrt(x)
  @inline begin
    is_negative = x < zero(x)
    unsafe_assume_condition(!is_negative)
    sqrt(x)
  end
end

@testset "UnsafeAssume.jl" begin
  @testset "sqrt" for T ∈ (Float16, Float32, Float64), n ∈ 0:5
    @test unsafe_sqrt(T(n*n)) == n
  end

  assumption_return_types(unsafe_assume_this_call_is_unreachable, Union{}, ())
  assumption_return_types(unsafe_assume_condition, (Bool,))

  @testset "unsafe_assume_integer_is_divisible_by_power_of_two: $I" for
  I ∈ IntegerTypes, M ∈ Masks
    assumption_return_types(unsafe_assume_integer_is_divisible_by_power_of_two, (I, M))
  end

  @testset "unsafe_assume_pointer_is_aligned: $P" for P ∈ PointerTypes, M ∈ Masks
    assumption_return_types(unsafe_assume_pointer_is_aligned, (P, M))
  end

  @testset "integer_is_divisible_by_power_of_two" begin
    @testset "some odd integers: $n" for n ∈ some_odd_integers, M ∈ Masks
      @test !integer_is_divisible_by_power_of_two(n, M())
    end

    @testset "0x1" for n ∈ 0:2:50
      @test integer_is_divisible_by_power_of_two(n, Val(0x1))
    end

    @testset "0x3" for n ∈ 0:4:50
      @test integer_is_divisible_by_power_of_two(n, Val(0x3))
    end

    @testset "0x7" for n ∈ 0:8:50
      @test integer_is_divisible_by_power_of_two(n, Val(0x7))
    end
  end
end
