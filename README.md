# UnsafeAssume

[![PkgEval](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/U/UnsafeAssume.svg)](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/U/UnsafeAssume.html)

Helps with convincing the Julia compiler (LLVM, to be specific) of the truth of some
condition, statement, predicate, observation etc. Using this is **dangerous**, only
use when certain that:

1. the forced assumption is correct, i.e., don't *lie to the compiler*

2. the performance benefit is significant for you

Due to the limitations of the current implementations of both Julia and LLVM, any
performance improvements achieved may be extremely fragile.

Implemented using raw LLVM assembly code with
[`Core.Intrinsics.llvmcall`](https://docs.julialang.org/en/v1/base/c/#Core.Intrinsics.llvmcall).

Similar functionality exists in C++ and Rust:

* https://en.cppreference.com/w/cpp/utility/unreachable

* https://en.cppreference.com/w/cpp/language/attributes/assume

* https://en.cppreference.com/w/cpp/memory/assume_aligned

* https://doc.rust-lang.org/std/hint/fn.unreachable_unchecked.html

## Usage examples

The calls of `UnsafeAssume` functions, along with all the code that establishes the
relevant assumptions, together with the code where applying the assumptions is
necessary for performance, should be within an `@inline` block. This ensures all
relevant information is available to the LLVM optimizer when the optimizer needs it.

### Preventing the domain check for `sqrt`

If we convince LLVM that a real floating-point argument to `sqrt` is nonnegative,
it may eliminate the code for throwing the domain error, resulting in the compiled
code consisting only of the essential square root instruction (`vsqrtsd`, in this
case), apart from the stack management instructions that any function needs to
have:

```julia
using UnsafeAssume

function unsafe_sqrt(x)
  @inline begin
    is_negative = x < zero(x)
    unsafe_assume_condition(!is_negative)
    sqrt(x)
  end
end
```

```julia-repl
julia> Base.print_statement_costs(stdout, unsafe_sqrt, Tuple{Float64})
unsafe_sqrt(x) @ Main REPL[2]:1
    0 1 ─ %1  = Base.lt_float(_2, 0.0)::Bool
    0 │   %2  = Base.not_int(%1)::Bool
    0 └──       goto #3 if not %2
    0 2 ─       goto #4
 1000 3 ─       (Core.Intrinsics.llvmcall)("unreachable", Union{}, Tuple{})::Union{}
    0 └──       unreachable
    0 4 ─ %7  = Base.lt_float(_2, 0.0)::Bool
    0 └──       goto #6 if not %7
    0 5 ─       invoke Base.Math.throw_complex_domainerror(:sqrt::Symbol, _2::Float64)::Union{}
    0 └──       unreachable
   20 6 ─ %11 = Base.Math.sqrt_llvm(_2)::Float64
    0 └──       goto #7
    0 7 ─       return %11
      

julia> @code_llvm unsafe_sqrt(0.3)
; Function Signature: unsafe_sqrt(Float64)
;  @ REPL[2]:1 within `unsafe_sqrt`
define double @julia_unsafe_sqrt_6500(double %"x::Float64") #0 {
top:
;  @ REPL[2]:3 within `unsafe_sqrt`
; ┌ @ float.jl:587 within `<`
   %0 = fcmp uge double %"x::Float64", 0.000000e+00
; └
;  @ REPL[2]:4 within `unsafe_sqrt`
; ┌ @ /some/path/UnsafeAssume/src/UnsafeAssume.jl:36 within `unsafe_assume_condition`
   call void @llvm.assume(i1 %0)
; └
;  @ REPL[2]:5 within `unsafe_sqrt`
; ┌ @ math.jl:687 within `sqrt`
   %1 = call double @llvm.sqrt.f64(double %"x::Float64")
; └
  ret double %1
}

julia> @code_native debuginfo=:none unsafe_sqrt(0.3)
        .text
        .file   "unsafe_sqrt"
        .globl  julia_unsafe_sqrt_6726          # -- Begin function julia_unsafe_sqrt_6726
        .p2align        4, 0x90
        .type   julia_unsafe_sqrt_6726,@function
julia_unsafe_sqrt_6726:                 # @julia_unsafe_sqrt_6726
; Function Signature: unsafe_sqrt(Float64)
# %bb.0:                                # %top
        #DEBUG_VALUE: unsafe_sqrt:x <- $xmm0
        push    rbp
        mov     rbp, rsp
        vsqrtsd xmm0, xmm0, xmm0
        pop     rbp
        ret
```

### Preventing an undefined reference check

Arrays with elements of `mutable` type may check whether the element is an
undefined reference on each access. This rarely matter for performance, but here's
how to prevent it anyway:

```julia
using UnsafeAssume

function unsafe_array_access(a)
  @inline begin
    unsafe_assume_condition(isassigned(a, 1))
    @inbounds a[1]
  end
end
```

```julia-repl
julia> a = [Int[]]  # This array type may contain undefined references because its elements are mutable
1-element Vector{Vector{Int64}}:
 []

julia> @code_llvm unsafe_array_access(a)
; Function Signature: unsafe_array_access(Array{Array{Int64, 1}, 1})
;  @ REPL[7]:1 within `unsafe_array_access`
define nonnull {}* @julia_unsafe_array_access_7773({}* noundef nonnull align 16 dereferenceable(40) %"a::Array") #0 {
top:
;  @ REPL[7]:3 within `unsafe_array_access`
; ┌ @ array.jl:268 within `isassigned`
; │┌ @ abstractarray.jl:684 within `checkbounds`
; ││┌ @ abstractarray.jl:386 within `eachindex`
; │││┌ @ abstractarray.jl:134 within `axes1`
; ││││┌ @ abstractarray.jl:98 within `axes`
; │││││┌ @ array.jl:191 within `size`
        %0 = bitcast {}* %"a::Array" to { i8*, i64, i16, i16, i32 }*
        %.length_ptr = getelementptr inbounds { i8*, i64, i16, i16, i32 }, { i8*, i64, i16, i16, i32 }* %0, i64 0, i32 1
        %.length = load i64, i64* %.length_ptr, align 8
; ││└└└└
; ││┌ @ abstractarray.jl:760 within `checkindex`
; │││┌ @ int.jl:513 within `<`
      %.not = icmp ne i64 %.length, 0
; │└└└
   call void @llvm.assume(i1 %.not)
; │ @ array.jl:270 within `isassigned`
   %1 = bitcast {}* %"a::Array" to {}***
   %.data5 = load {}**, {}*** %1, align 8
   %array_slot = load atomic {}*, {}** %.data5 unordered, align 8
; └
;  @ REPL[7]:4 within `unsafe_array_access`
  ret {}* %array_slot
}

julia> @code_native debuginfo=:none unsafe_array_access(a)
        .text
        .file   "unsafe_array_access"
        .globl  julia_unsafe_array_access_7783  # -- Begin function julia_unsafe_array_access_7783
        .p2align        4, 0x90
        .type   julia_unsafe_array_access_7783,@function
julia_unsafe_array_access_7783:         # @julia_unsafe_array_access_7783
; Function Signature: unsafe_array_access(Array{Array{Int64, 1}, 1})
# %bb.0:                                # %top
        #DEBUG_VALUE: unsafe_array_access:a <- [DW_OP_deref] $rdi
        #DEBUG_VALUE: unsafe_array_access:a <- [DW_OP_deref] 0
        push    rbp
        mov     rax, qword ptr [rdi]
        mov     rbp, rsp
        mov     rax, qword ptr [rax]
        pop     rbp
        ret
```

### Informing the compiler that given arrays don't overlap

The possibility for the backing memory regions of two different arrays to overlap is
sometimes called *aliasing*. The generated code may be much worse when the compiler
can't exclude this possibility for a function that operates on both arrays, which
is, e.g., why C99 introduced the `restrict` keyword into the C language. This
example shows how to use UnsafeAssume.jl to inform Julia that there's no aliasing
between two arrays. Warning: the `Base.mightalias` function, used here to check for
aliasing, is not a public API of Julia's `Base`. The AllocCheck.jl package is used
to show that there's no allocations in the version of the function that uses
UnsafeAssume.jl, which is just one of the performance improvements that can be
achieved in a case like this.

```julia
using UnsafeAssume

f!(x, y) = @inline x .+= y

function f_noalias!(x, y)
  @inline begin
    unsafe_assume_condition(!Base.mightalias(x, y))
    x .+= y
  end
end
```

```julia-repl
julia> using AllocCheck

julia> check_allocs(f_noalias!, Tuple{Vector{Int},Vector{Int}})
Any[]

julia> check_allocs(f!, Tuple{Vector{Int},Vector{Int}})
1-element Vector{Any}:
 Allocation of Array in ./array.jl:365
  | copy(a::T) where {T<:Array} = ccall(:jl_array_copy, Ref{T}, (Any,), a)

Stacktrace:
  [1] copy
    @ ./array.jl:365 [inlined]
  [2] unaliascopy
    @ ./abstractarray.jl:1498 [inlined]
  [3] unalias
    @ ./abstractarray.jl:1482 [inlined]
  [4] broadcast_unalias
    @ ./broadcast.jl:947 [inlined]
  [5] preprocess
    @ ./broadcast.jl:954 [inlined]
  [6] preprocess_args
    @ ./broadcast.jl:957 [inlined]
  [7] preprocess_args
    @ ./broadcast.jl:956 [inlined]
  [8] preprocess
    @ ./broadcast.jl:953 [inlined]
  [9] copyto!
    @ ./broadcast.jl:970 [inlined]
 [10] copyto!
    @ ./broadcast.jl:926 [inlined]
 [11] materialize!
    @ ./broadcast.jl:884 [inlined]
 [12] materialize!
    @ ./broadcast.jl:881 [inlined]
 [13] f!(x::Vector{Int64}, y::Vector{Int64})
    @ Main ./REPL[2]:1
```

### Example with multiple assumptions

If there are multiple conditions, and we want to assume they all hold, call an
`unsafe_assume_` function for each condition. In this example, suppose we have a
collection whose element type is not known (e.g., `Vector{Any}`), and we want to
assume that an element exists and it's an `Int`. Here's how to do this without any
throwing in the generated machine code:

```julia
using UnsafeAssume

"""
Return the first value from a collection, assuming it exists.
"""
function first_unsafe(coll)
  @inline begin
    unsafe_assume_condition(0 < length(coll))
    unsafe_assume_condition(isassigned(coll, 1))
    first(coll)
  end
end

"""
Return the first value from a collection, assuming it exists and it's an `Int`.
"""
function first_int_unsafe(coll)
  @inline begin
    ret = first_unsafe(coll)
    unsafe_assume_condition(ret isa Int)
    ret::Int
  end
end
```

```julia-repl
julia> @code_native debuginfo=:none first_int_unsafe(Any[])
        .text
        .file   "first_int_unsafe"
        .globl  julia_first_int_unsafe_624      # -- Begin function julia_first_int_unsafe_624
        .p2align        4, 0x90
        .type   julia_first_int_unsafe_624,@function
julia_first_int_unsafe_624:             # @julia_first_int_unsafe_624
; Function Signature: first_int_unsafe(Array{Any, 1})
# %bb.0:                                # %top
        #DEBUG_VALUE: first_int_unsafe:coll <- [DW_OP_deref] $rdi
        #DEBUG_VALUE: first_int_unsafe:coll <- [DW_OP_deref] 0
        push    rbp
        mov     rax, qword ptr [rdi]
        mov     rbp, rsp
        mov     rax, qword ptr [rax]
        mov     rax, qword ptr [rax]
        pop     rbp
        ret
```
